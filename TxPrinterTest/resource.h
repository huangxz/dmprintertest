//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 TxPrinterTest.rc 使用
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TXPRINTERTEST_DIALOG        102
#define IDS_STRING102                   102
#define IDS_STRING103                   103
#define IDS_STRING104                   104
#define IDS_STRING105                   105
#define IDS_STRING106                   106
#define IDS_STRING107                   107
#define IDS_STRING108                   108
#define IDS_STRING109                   109
#define IDS_STRING110                   110
#define IDD_SENDHEXDATA                 111
#define IDS_STRING111                   111
#define IDD_DENDLG                      112
#define IDS_STRING112                   112
#define IDS_STRING113                   113
#define IDS_STRING114                   114
#define IDS_STRING115                   115
#define IDS_STRING116                   116
#define IDS_STRING117                   117
#define IDS_STRING118                   118
#define IDS_STRING119                   119
#define IDS_STRING120                   120
#define IDS_STRING121                   121
#define IDS_STRING122                   122
#define IDS_STRING123                   123
#define IDS_STRING124                   124
#define IDS_STRING125                   125
#define IDR_MAINFRAME                   128
#define IDC_COMBO1                      1000
#define IDC_COMBO2                      1001
#define IDC_BUTTON1                     1002
#define IDC_BUTTON2                     1003
#define IDC_BUTTON3                     1004
#define IDC_BUTTON4                     1005
#define IDC_BUTTON5                     1006
#define IDC_BUTTON6                     1007
#define IDC_BUTTON7                     1008
#define IDC_BUTTON8                     1009
#define IDC_BUTTON9                     1010
#define IDC_BUTTON10                    1011
#define IDC_BUTTON11                    1012
#define IDC_BUTTON12                    1013
#define IDC_BUTTON13                    1014
#define IDC_BUTTON14                    1015
#define IDC_BUTTON15                    1016
#define IDC_BUTTON16                    1017
#define IDC_BUTTON17                    1018
#define IDC_BUTTON18                    1019
#define IDC_BUTTON19                    1020
#define IDC_BUTTON20                    1021
#define IDC_BUTTON21                    1022
#define IDC_BUTTON22                    1023
#define IDC_BUTTON23                    1024
#define IDC_BUTTON24                    1025
#define IDC_COMBO3                      1026
#define IDC_BUTTON25                    1027
#define IDC_BUTTON26                    1028
#define IDC_EDIT100                     1029
#define IDC_BUTTON29                    1029
#define IDC_COMBO5                      1029
#define IDC_BUTTON27                    1030
#define IDC_COMBO200                    1031
#define IDC_BUTTON30                    1031
#define IDC_COMBO6                      1031
#define IDC_STATIC2                     1032
#define IDC_BUTTON34                    1032
#define IDC_BUTTON1000                  1033
#define IDC_BUTTON31                    1033
#define IDC_COMBO7                      1033
#define IDC_BUTTON28                    1034
#define IDC_COMBO8                      1034
#define IDC_BUTTON32                    1035
#define IDC_COMBO9                      1035
#define IDC_BUTTON35                    1036
#define IDC_BUTTON36                    1037
#define IDC_BUTTON37                    1038
#define IDC_BUTTON38                    1039
#define IDC_BUTTON39                    1040
#define IDC_BUTTON40                    1041
#define IDC_BUTTON41                    1042
#define IDC_SPIN1                       1045
#define IDC_EDIT_PAGE                   1046
#define IDC_EDIT_DELAY                  1047
#define IDC_SPIN2                       1048
#define IDC_BUTTON33                    1051
#define IDC_COMBO4                      1052
#define IDC_STATIC1                     1053

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1054
#define _APS_NEXT_SYMED_VALUE           113
#endif
#endif
