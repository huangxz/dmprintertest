
// TxPrinterTestDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "TxPrinterTest.h"
#include "TxPrinterTestDlg.h"
#include "afxdialogex.h"
//#include<tchar.h>
#include "afxwin.h"
#include <windows.h>
#include <stdio.h>
#include "export.h"
#include <new.h>
#include <exception> 
#include <vector>

#pragma comment(lib, "TxPrnMod.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	// 实现
protected:
	DECLARE_MESSAGE_MAP()
public:
	//	afx_msg void OnBnDoubleclickedButton2();
	//	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//	bool eggshell;
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnDoubleclickedButton1000();
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
	//	, eggshell(false)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
	//	ON_BN_DOUBLECLICKED(IDC_BUTTON2, &CAboutDlg::OnBnDoubleclickedButton2)
	//	ON_WM_CTLCOLOR()
	//	ON_BN_CLICKED(IDC_BUTTON2, &CAboutDlg::OnBnClickedButton2)
	//	ON_BN_DOUBLECLICKED(IDC_BUTTON1000, &CAboutDlg::OnBnDoubleclickedButton1000)
END_MESSAGE_MAP()


// CTxPrinterTestDlg 对话框



CTxPrinterTestDlg::CTxPrinterTestDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTxPrinterTestDlg::IDD, pParent)	
	, ifopen(false)
	//	, m_Status(_T(""))
	,Chinese("人生若只如初见 。--纳兰性德")
	,English("If the very first glance could last forever. by Nalan Xingde")
	,number("1234567890")
	//, mypage(0)
	//, mydelay(0)
	//, mypage(0)
	//, mydelay(0)
	, Counter(0)
	, Stabuff(NULL)
	, delay(0)
	, page(0)
	, fLen(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTxPrinterTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	//	DDX_Control(pDX, IDC_COMBO1, m_combobox);
	//	DDX_CBString(pDX, IDC_COMBO1, m_port);
	//	DDX_CBString(pDX, IDC_COMBO2, m_baud);
	//	DDX_Text(pDX, IDC_EDIT1, m_Status);
	//DDX_Control(pDX, IDC_SCROLLBAR1, page);
	//DDX_Control(pDX, IDC_SCROLLBAR2, delay);
	//	DDX_Text(pDX, IDC_EDIT1, mypage);
	//	DDX_Text(pDX, IDC_EDIT2, mydelay);
//	DDX_Text(pDX, IDC_EDIT_DELAY, delay);
//	DDX_Text(pDX, IDC_EDIT_PAGE, page);
}

BEGIN_MESSAGE_MAP(CTxPrinterTestDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	//	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//	ON_CBN_SELCHANGE(IDC_COMBO1, &CTxPrinterTestDlg::OnCbnSelchangeCombo1)
	//ON_CBN_SELCHANGE(IDC_COMBO1, &CTxPrinterTestDlg::OnCbnSelchangeCombo1)
	ON_BN_CLICKED(IDC_BUTTON25, &CTxPrinterTestDlg::OnBnClickedButton25)
	ON_BN_CLICKED(IDC_BUTTON1, &CTxPrinterTestDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CTxPrinterTestDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON26, &CTxPrinterTestDlg::OnBnClickedButton26)
	ON_BN_CLICKED(IDC_BUTTON3, &CTxPrinterTestDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON5, &CTxPrinterTestDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON18, &CTxPrinterTestDlg::OnBnClickedButton18)
	ON_BN_CLICKED(IDC_BUTTON24, &CTxPrinterTestDlg::OnBnClickedButton24)
	ON_BN_CLICKED(IDC_BUTTON9, &CTxPrinterTestDlg::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON4, &CTxPrinterTestDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON22, &CTxPrinterTestDlg::OnBnClickedButton22)
	ON_BN_CLICKED(IDC_BUTTON7, &CTxPrinterTestDlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON8, &CTxPrinterTestDlg::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON10, &CTxPrinterTestDlg::OnBnClickedButton10)
	ON_BN_CLICKED(IDC_BUTTON11, &CTxPrinterTestDlg::OnBnClickedButton11)
	ON_BN_CLICKED(IDC_BUTTON12, &CTxPrinterTestDlg::OnBnClickedButton12)
	ON_BN_CLICKED(IDC_BUTTON13, &CTxPrinterTestDlg::OnBnClickedButton13)
	ON_BN_CLICKED(IDC_BUTTON14, &CTxPrinterTestDlg::OnBnClickedButton14)
	ON_BN_CLICKED(IDC_BUTTON15, &CTxPrinterTestDlg::OnBnClickedButton15)
	ON_BN_CLICKED(IDC_BUTTON16, &CTxPrinterTestDlg::OnBnClickedButton16)
	ON_BN_CLICKED(IDC_BUTTON17, &CTxPrinterTestDlg::OnBnClickedButton17)
	ON_BN_CLICKED(IDC_BUTTON19, &CTxPrinterTestDlg::OnBnClickedButton19)
	ON_BN_CLICKED(IDC_BUTTON6, &CTxPrinterTestDlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON20, &CTxPrinterTestDlg::OnBnClickedButton20)
	//ON_BN_CLICKED(IDC_BUTTON21, &CTxPrinterTestDlg::OnBnClickedButton21)
	ON_BN_CLICKED(IDC_BUTTON21, &CTxPrinterTestDlg::OnBnClickedButton21)
	ON_BN_CLICKED(IDC_BUTTON23, &CTxPrinterTestDlg::OnBnClickedButton23)
	//ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON27, &CTxPrinterTestDlg::OnBnClickedButton27)
	//ON_WM_VSCROLL()
	//ON_EN_CHANGE(IDC_EDIT1, &CTxPrinterTestDlg::OnEnChangeEdit1)
	//ON_NOTIFY(NM_THEMECHANGED, IDC_SCROLLBAR1, &CTxPrinterTestDlg::OnNMThemeChangedScrollbar1)
	//ON_WM_LBUTTONDOWN()
	//ON_EN_CHANGE(IDC_EDIT2, &CTxPrinterTestDlg::OnEnChangeEdit2)
	ON_BN_CLICKED(IDC_BUTTON29, &CTxPrinterTestDlg::OnBnClickedButton29)
	ON_BN_CLICKED(IDC_BUTTON30, &CTxPrinterTestDlg::OnBnClickedButton30)
	ON_BN_CLICKED(IDC_BUTTON28, &CTxPrinterTestDlg::OnBnClickedButton28)
	ON_BN_CLICKED(IDC_BUTTON31, &CTxPrinterTestDlg::OnBnClickedButton31)
	ON_BN_CLICKED(IDC_BUTTON32, &CTxPrinterTestDlg::OnBnClickedButton32)
	ON_WM_TIMER()
	ON_WM_PAINT()
//	ON_EN_CHANGE(IDC_EDIT_PAGE, &CTxPrinterTestDlg::OnEnChangeEditPage)
//	ON_EN_CHANGE(IDC_EDIT_DELAY, &CTxPrinterTestDlg::OnEnChangeEditDelay)
	ON_CBN_SELCHANGE(IDC_COMBO3, &CTxPrinterTestDlg::OnCbnSelchangeCombo3)
	//ON_BN_CLICKED(IDC_BUTTON33, &CTxPrinterTestDlg::OnBnClickedButton33)
	//ON_BN_CLICKED(IDC_BUTTON34, &CTxPrinterTestDlg::OnBnClickedButton34)
	//ON_BN_CLICKED(IDC_BUTTON35, &CTxPrinterTestDlg::OnBnClickedButton35)
	//ON_BN_CLICKED(IDC_BUTTON36, &CTxPrinterTestDlg::OnBnClickedButton36)
	//ON_BN_CLICKED(IDC_BUTTON37, &CTxPrinterTestDlg::OnBnClickedButton37)
	//ON_BN_CLICKED(IDC_BUTTON38, &CTxPrinterTestDlg::OnBnClickedButton38)
	//ON_BN_CLICKED(IDC_BUTTON39, &CTxPrinterTestDlg::OnBnClickedButton39)
	//ON_BN_CLICKED(IDC_BUTTON40, &CTxPrinterTestDlg::OnBnClickedButton40)
	//ON_BN_CLICKED(IDC_BUTTON41, &CTxPrinterTestDlg::OnBnClickedButton41)
	//ON_CBN_SELCHANGE(IDC_COMBO4, &CTxPrinterTestDlg::OnCbnSelchangeCombo4)
	ON_WM_CTLCOLOR()
	ON_CBN_SELCHANGE(IDC_COMBO5, &CTxPrinterTestDlg::OnCbnSelchangeCombo5)
	ON_CBN_SELCHANGE(IDC_COMBO4, &CTxPrinterTestDlg::OnCbnSelchangeCombo4)
	ON_CBN_SELCHANGE(IDC_COMBO6, &CTxPrinterTestDlg::OnCbnSelchangeCombo6)
	ON_CBN_SELCHANGE(IDC_COMBO7, &CTxPrinterTestDlg::OnCbnSelchangeCombo7)
	ON_CBN_SELCHANGE(IDC_COMBO8, &CTxPrinterTestDlg::OnCbnSelchangeCombo8)
	ON_CBN_SELCHANGE(IDC_COMBO9, &CTxPrinterTestDlg::OnCbnSelchangeCombo9)
	ON_BN_CLICKED(IDC_BUTTON33, &CTxPrinterTestDlg::OnBnClickedButton33)
	ON_BN_CLICKED(IDC_BUTTON34, &CTxPrinterTestDlg::OnBnClickedButton34)
	ON_BN_CLICKED(IDC_BUTTON35, &CTxPrinterTestDlg::OnBnClickedButton35)
	ON_BN_CLICKED(IDC_BUTTON39, &CTxPrinterTestDlg::OnBnClickedButton39)
	ON_BN_CLICKED(IDC_BUTTON40, &CTxPrinterTestDlg::OnBnClickedButton40)
	ON_BN_CLICKED(IDC_BUTTON37, &CTxPrinterTestDlg::OnBnClickedButton37)
	ON_BN_CLICKED(IDC_BUTTON38, &CTxPrinterTestDlg::OnBnClickedButton38)
	ON_BN_CLICKED(IDC_BUTTON36, &CTxPrinterTestDlg::OnBnClickedButton36)
END_MESSAGE_MAP()


// CTxPrinterTestDlg 消息处理程序

BOOL CTxPrinterTestDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标


	((CComboBox*)GetDlgItem(IDC_COMBO3))->SetCurSel(3);
	((CComboBox*)GetDlgItem(IDC_COMBO4))->SetCurSel(9);


	// TODO: 在此添加额外的初始化代码
	((CComboBox*)GetDlgItem(IDC_COMBO1))->SetCurSel(0);
	((CComboBox*)GetDlgItem(IDC_COMBO2))->SetCurSel(0);
	((CComboBox*)GetDlgItem(IDC_COMBO5))->SetCurSel(0);
	((CComboBox*)GetDlgItem(IDC_COMBO6))->SetCurSel(0);
	((CComboBox*)GetDlgItem(IDC_COMBO7))->SetCurSel(0);
	((CComboBox*)GetDlgItem(IDC_COMBO8))->SetCurSel(0);
	((CComboBox*)GetDlgItem(IDC_COMBO9))->SetCurSel(0);

//	(GetDlgItem(IDC_EDIT_PAGE)->SetWindowTextW(_T("1")));
//	(GetDlgItem(IDC_EDIT_DELAY)->SetWindowTextW(_T("0")));
	UpdateData(TRUE);

//	((CSpinButtonCtrl*)GetDlgItem(IDC_SPIN1))->SetRange(1,500);
//	((CSpinButtonCtrl*)GetDlgItem(IDC_SPIN1))->SetBuddy(this->GetDlgItem(IDC_EDIT_PAGE));

//	((CSpinButtonCtrl*)GetDlgItem(IDC_SPIN2))->SetRange(0,60);
//	((CSpinButtonCtrl*)GetDlgItem(IDC_SPIN2))->SetBuddy(this->GetDlgItem(IDC_EDIT_DELAY));
	//((CListBox*)GetDlgItem(IDC_LIST1))->AddString(_T("1"));
	//((CListBox*)GetDlgItem(IDC_LIST1))->AddString(_T("2"));
	////((CListBox*)GetDlgItem(IDC_LIST1))->AddString(_T("3"));
	//((CListBox*)GetDlgItem(IDC_LIST1))->AddString(_T("4"));
	//((CListBox*)GetDlgItem(IDC_LIST1))->AddString(_T("5"));

	//page.SetScrollRange(1,500);
	//page.SetScrollPos(1);
	//SetDlgItemInt(IDC_EDIT1,1);

	//delay.SetScrollRange(0,60);
	//delay.SetScrollPos(0);
	//SetDlgItemInt(IDC_EDIT2,0);
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CTxPrinterTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CTxPrinterTestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CTxPrinterTestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



//void CTxPrinterTestDlg::OnCbnSelchangeCombo1()
//{
//	// TODO: 在此添加控件通知处理程序代码
//}




void CTxPrinterTestDlg::OnBnClickedButton25()
{
	// TODO: 在此添加控件通知处理程序代码
	CAboutDlg dlg;
	dlg.DoModal(); 
}


void CTxPrinterTestDlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码

	CString CSm_port,CSm_baud;
	if (ifopen==true) {AfxMessageBox(IDS_STRING102); return;}
	((CComboBox*)GetDlgItem(IDC_COMBO1))->GetWindowText(CSm_port);
	((CComboBox*)GetDlgItem(IDC_COMBO2))->GetWindowText(CSm_baud);

	if (CSm_port.Left(3)==L"USB") 
	{m_port=TX_TYPE_USB; m_portIdx=0;}

	if(CSm_port.Left(3)==L"LPT")  
	{m_port=TX_TYPE_LPT; m_portIdx=_tstoi((LPCTSTR)(CSm_port.Right(1)))-1;}
	if (CSm_port.Left(3)==L"COM") 
	{
		m_port=TX_TYPE_COM;
		m_portIdx=_tstoi((LPCTSTR)(CSm_port.Right(1)))-1;
		switch(_tstoi((LPCTSTR)(CSm_baud)))
		{case 9600:m_baud=TX_SER_BAUD9600;break;
		case 19200:m_baud=TX_SER_BAUD19200;break;
		case 38400:m_baud=TX_SER_BAUD38400;break;
		case 57600:m_baud=TX_SER_BAUD57600;break;
		case 115200:m_baud=TX_SER_BAUD115200;break;}
	}

	int result;
	result=TxOpenPrinter(m_port,m_portIdx);	
	if (result==0)
	{
		AfxMessageBox(IDS_STRING103);
		return;
	}

	if (result<0)
	{
		AfxMessageBox(IDS_STRING104);
		return;
	}
	//result>0		
	AfxMessageBox(IDS_STRING105);
	ifopen=true;
//	((CComboBox*)GetDlgItem(IDC_COMBO3))->EnableWindow(TRUE);
//	((CComboBox*)GetDlgItem(IDC_COMBO4))->EnableWindow(TRUE);

	if (m_port==TX_TYPE_COM)
	{
		TxSetupSerial(m_baud|TX_SER_DATA_8BITS|TX_SER_PARITY_NONE|TX_SER_STOP_1BITS|TX_SER_FLOW_HARD);
	}


	TxInit();
	

	//}
	//	else {AfxMessageBox(IDS_STRING106);}
}


void CTxPrinterTestDlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	ifopen=false;
//	((CComboBox*)GetDlgItem(IDC_COMBO3))->EnableWindow(FALSE);
//	((CComboBox*)GetDlgItem(IDC_COMBO4))->EnableWindow(FALSE);
	TxClosePrinter();


	AfxMessageBox(IDS_STRING108);
}


void CTxPrinterTestDlg::OnBnClickedButton26()
{
	// TODO: 在此添加控件通知处理程序代码
	//	m_Status=_T("");
	//	UpdateData(false);
	//if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	CSendHexData  dlg;
	dlg.DoModal();
}


void CTxPrinterTestDlg::OnBnClickedButton3()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	//1X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_1X,TX_SIZE_1X);
	TxOutputStringLn(Chinese);

	//2X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_1X,TX_SIZE_2X);
	TxOutputStringLn(Chinese);

	//3X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_1X,TX_SIZE_3X);
	TxOutputStringLn(Chinese);

	//4X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_1X,TX_SIZE_4X);
	TxOutputStringLn(Chinese);

	//5X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_1X,TX_SIZE_5X);
	TxOutputStringLn(Chinese);

	//6X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_1X,TX_SIZE_6X);
	TxOutputStringLn(Chinese);

	//7X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_1X,TX_SIZE_7X);
	TxOutputStringLn(Chinese);

	//8X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_1X,TX_SIZE_8X);
	TxOutputStringLn(Chinese);
	TxDoFunction(TX_FEED,220,0);

	TxInit();

}


void CTxPrinterTestDlg::OnBnClickedButton5()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}

	TxOutputStringLn(Chinese);
	TxOutputStringLn(English);
	TxOutputStringLn(number);

	TxDoFunction(TX_FEED,220,0);

	TxDoFunction(TX_FONT_ULINE,TX_ON,0);



	TxOutputStringLn(Chinese);
	TxOutputStringLn(English);
	TxOutputStringLn(number);

	TxDoFunction(TX_FEED,220,0);

	TxInit();

}


void CTxPrinterTestDlg::OnBnClickedButton18()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}

	TxDoFunction(TX_CUT,TX_CUT_FULL,0);

}




void CTxPrinterTestDlg::OnBnClickedButton24()
{
	// TODO: 在此添加控件通知处理程序代码
	int Result;
	CString Out;
	bool normal(true);
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	Result=TxGetStatus();
	if (m_port==TX_TYPE_USB) 
	{
		Result=TxGetStatus2(); 
	}
	//result=TxGetStatus2();
	Out.Format(_T("%x"),Result);
	Out=_T("0x")+Out+_T("\r\n");
	if (Result&TX_STAT_PAPEREND) {Out+=_T("打印机缺纸 "); normal=false;}
	if (Result&TX_STAT_BUSY) {Out+=_T("打印机繁忙"); normal=false;}
	if (Result&TX_STAT_COVER) {Out+=_T("打印机机芯的盖子打开 "); normal=false;}
	if (Result&TX_STAT_ERROR) {Out+=_T("打印机错误 "); normal=false;}
	if (Result&TX_STAT_RCV_ERR) {Out+=_T("有需人工恢复的可恢复错误 "); normal=false;}
	if (Result&TX_STAT_CUT_ERR) {Out+=_T("有切刀错误 "); normal=false;}
	if (Result&TX_STAT_URCV_ERR) {Out+=_T("有不可恢复错误 "); normal=false;}
	if (Result&TX_STAT_ARCV_ERR) {Out+=_T("有可自动恢复的错误 "); normal=false;}
	if (Result&TX_STAT_PAPER_NE) {Out+=_T("纸将近 "); normal=false;}
	if (normal) {Out+=_T("打印机状态正常");}
	AfxMessageBox(Out);


	//	m_Status.Format(_T("%x"),result);
	//	m_Status=_T("0x")+m_Status;
	//	UpdateData(false);

}


void CTxPrinterTestDlg::OnBnClickedButton9()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	CFileDialog dlg(TRUE,NULL,NULL,OFN_FILEMUSTEXIST|OFN_FORCESHOWHIDDEN,_T("支持图片格式|*.bmp;*.cut;*.dds;*.exr;*.g3;*.gif;*.hdr;*.ico;*.iff;*.jbg;*jbig;*.jng;*.jpg;*.jpeg;*.wdp;*.hdp;*.jxr;*.pcd;*.mng;*.pcx;*.pbm;*.pgm;*.ppm;*.pnm;*.pfm;*.png;*.pict;*.psd;*.raw;*.crw;*.cr2;*.nef;*.raf;*.dng;*.mos;*.kdc;*.dcr;*.ras;*.sgi;*.rgb;*.rgba;*.bw;*.int;*.inta;*.tga;*.tiff;*.tif;*.wbmp;*.webp;*.xbm;*.xpm||"),AfxGetMainWnd(),NULL,1);
	if(dlg.DoModal() == IDOK) 
	{
		CStringA path(dlg.GetPathName()); 
		if (TxPrintImage(path.GetString())<=0) {AfxMessageBox(IDS_STRING110);}
		else {TxDoFunction(TX_FEED,220,0);}
	}
}


void CTxPrinterTestDlg::OnBnClickedButton4()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	//1X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_1X,TX_SIZE_1X);
	TxOutputStringLn(Chinese);

	//2X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_2X,TX_SIZE_1X);
	TxOutputStringLn(Chinese);

	//3X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_3X,TX_SIZE_1X);
	TxOutputStringLn(Chinese);

	//4X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_4X,TX_SIZE_1X);
	TxOutputStringLn(Chinese);

	//5X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_5X,TX_SIZE_1X);
	TxOutputStringLn(Chinese);

	//6X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_6X,TX_SIZE_1X);
	TxOutputStringLn(Chinese);

	//7X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_7X,TX_SIZE_1X);
	TxOutputStringLn(Chinese);

	//8X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_8X,TX_SIZE_1X);
	TxOutputStringLn(Chinese);
	TxDoFunction(TX_FEED,220,0);

	TxInit();
}


void CTxPrinterTestDlg::OnBnClickedButton22()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}

	TxOutputStringLn(Chinese);
	TxOutputStringLn(English);
	TxOutputStringLn(number);

	TxDoFunction(TX_FEED,220,0);

	TxDoFunction(TX_FONT_BOLD,TX_ON,0);

	TxOutputStringLn(Chinese);
	TxOutputStringLn(English);
	TxOutputStringLn(number);

	TxDoFunction(TX_FEED,220,0);

	TxInit();
}


void CTxPrinterTestDlg::OnBnClickedButton7()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}

	TxDoFunction(TX_LINE_SP,30,0);
	TxOutputStringLn("-----行距30点(3.75mm)-----");
	TxOutputStringLn("------------------------------");
	TxOutputStringLn("------------------------------");

	TxDoFunction(TX_FEED,100,0);

	TxDoFunction(TX_LINE_SP,40,0);
	TxOutputStringLn("-----行距40点(5mm)-----");
	TxOutputStringLn("------------------------------");
	TxOutputStringLn("------------------------------");

	TxDoFunction(TX_FEED,100,0);

	TxDoFunction(TX_LINE_SP,50,0);
	TxOutputStringLn("-----行距50点(6.25mm)-----");
	TxOutputStringLn("------------------------------");
	TxOutputStringLn("------------------------------");

	TxDoFunction(TX_FEED,100,0);

	TxDoFunction(TX_LINE_SP,600,0);
	TxOutputStringLn("-----行距60点(7.5mm)-----");
	TxOutputStringLn("------------------------------");
	TxOutputStringLn("------------------------------");

	TxDoFunction(TX_FEED,100,0);

	TxDoFunction(TX_LINE_SP,70,0);
	TxOutputStringLn("-----行距70点(8.75mm)-----");
	TxOutputStringLn("------------------------------");
	TxOutputStringLn("------------------------------");



	TxDoFunction(TX_FEED,220,0);

	TxInit();
}


void CTxPrinterTestDlg::OnBnClickedButton8()
{
	// TODO: 在此添加控件通知处理程序代码
	//CFileDialog dlg(TRUE,NULL,NULL,OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,"All Files(*.TXT)|*.TXT||",AfxGetMainWnd());
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	CFileDialog dlg(TRUE,NULL,NULL,OFN_FILEMUSTEXIST|OFN_FORCESHOWHIDDEN,NULL,AfxGetMainWnd(),NULL,1);
	CString strPath,strText=_T("");
	if(dlg.DoModal() == IDOK)
	{
		strPath = dlg.GetPathName();
		CFile file(strPath,CFile::modeRead);
		fLen=(DWORD)file.GetLength();
		char *buff;
		try
		{
			buff=new char[fLen+1];
			file.Read(buff,fLen);
			file.Close();
			buff[fLen]=0;
			//TxOutputStringLn(buff);
			TxWritePrinter(buff,fLen);
			delete []buff;
		}
		catch (std::bad_alloc&)
		{
			AfxMessageBox(IDS_STRING109); 
			return;
		}


	}
}


void CTxPrinterTestDlg::OnBnClickedButton10()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	//TxPrintImage("\\a1.png");
	TxDoFunction(TX_PRINT_LOGO,TX_LOGO_1X1,0);
	TxDoFunction(TX_FEED,220,0);
}


void CTxPrinterTestDlg::OnBnClickedButton11()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	CString temp;
	using std::vector;
	vector<char> Out(6);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x77;
	Out[3] = 0x0;
	Out[4] = 0x1;
	Out[5] = 0x0;
	TxWritePrinter(&Out[0], Out.size());
	TxInit();

/*	TxDoFunction(TX_BARCODE_FONT,TX_BAR_FONT_DOWN,0);

	TxOutputStringLn("----------UPCA----------");
	TxPrintBarcode(TX_BAR_UPCA,"12345678901");
	TxOutputStringLn("----------UPCA----------");
	TxDoFunction(TX_FEED,100,0);
===========long long ago=========================================
	TxOutputStringLn("----------UPCE----------");
	TxPrintBarcode(TX_BAR_UPCE,"123643");
	TxPrintBarcode(TX_BAR_UPCE,"1");
	TxPrintBarcode(TX_BAR_UPCE,"12");
	TxPrintBarcode(TX_BAR_UPCE,"123");
	TxPrintBarcode(TX_BAR_UPCE,"1234");
	TxPrintBarcode(TX_BAR_UPCE,"12345");
	TxPrintBarcode(TX_BAR_UPCE,"123456");
	TxPrintBarcode(TX_BAR_UPCE,"1234567");
	TxPrintBarcode(TX_BAR_UPCE,"12345678");
	TxPrintBarcode(TX_BAR_UPCE,"123456789");
	TxPrintBarcode(TX_BAR_UPCE,"1234567890");
	TxOutputStringLn("----------UPCE----------");
	TxDoFunction(TX_FEED,100,0);
=======================================================	
	TxDoFunction(TX_BARCODE_WIDTH,2,0);
	TxOutputStringLn("----------EAN13----------");
	TxPrintBarcode(TX_BAR_EAN13,"690190908428");
	TxOutputStringLn("----------EAN13----------");
	TxDoFunction(TX_FEED,100,0);

	TxDoFunction(TX_BARCODE_WIDTH,3,0);

	TxOutputStringLn("----------EAN8----------");
	TxPrintBarcode(TX_BAR_EAN8,"1234567");
	TxOutputStringLn("----------EAN8----------");
	TxDoFunction(TX_FEED,100,0);

	TxOutputStringLn("----------CODE39----------");
	TxPrintBarcode(TX_BAR_CODE39,"12345");
	TxOutputStringLn("----------CODE39----------");
	TxDoFunction(TX_FEED,100,0);

	TxOutputStringLn("----------ITF----------");
	TxPrintBarcode(TX_BAR_ITF,"1234567890");
	TxOutputStringLn("----------ITF----------");
	TxDoFunction(TX_FEED,100,0);

	TxOutputStringLn("----------CODABAR----------");
	TxPrintBarcode(TX_BAR_CODABAR,"A123456A");
	TxOutputStringLn("----------CODABAR----------");
	TxDoFunction(TX_FEED,100,0);

	TxOutputStringLn("----------CODE93----------");
	TxPrintBarcode(TX_BAR_CODE93,"A1234A");
	TxOutputStringLn("----------CODE93----------");
	TxDoFunction(TX_FEED,100,0);

	//char code[10];
	//code[1]=char(0x76);
	//code[2]=char(0x42);
	//code[3]=char(0x4E);
	//code[4]=char(0x6F);
	//code[5]=char(0x2E);
	//code[6]=char(0x7B);
	//code[7]=char(0x43);
	//code[8]=char(0x0c);
	//code[9]=char(0x22);
	//code[10]=char(0x38);
	char code[]={char(0x76),char(0x42),char(0x4E),char(0x6F),char(0x2E),char(0x7B),(0x43),char(0x0c),char(0x22),char(0x38)};
	TxOutputStringLn("----------CODE128----------");
	TxPrintBarcode(TX_BAR_CODE128,code);
	TxOutputStringLn("----------CODE128----------");
	TxDoFunction(TX_FEED,220,0);

*/
}


void CTxPrinterTestDlg::OnBnClickedButton12()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	CString temp;
	using std::vector;
	vector<char> Out(6);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x77;
	Out[3] = 0x0;
	Out[4] = 0x1;
	Out[5] = 0x1;
	TxWritePrinter(&Out[0], Out.size());
	TxInit();
/*	char* url="http://www.baidu.com";
	//char* output=new char[100];
	//Magic number!!
	//memset(output,0,sizeof(output));
	//strcat_s(output,100,Chinese);
	//strcat_s(output,100,English);
	//strcat_s(output,100,number);
	TxDoFunction(TX_HOR_POS,80,0);
	TxDoFunction(TX_QR_ERRLEVEL,TX_QR_ERRLEVEL_M,0);
	TxDoFunction(TX_QR_DOTSIZE,6,0);
	//TxPrintQRcode(output,strlen(output));
	TxPrintQRcode(url,strlen(url));
	TxDoFunction(TX_FEED,100,0);

	TxDoFunction(TX_HOR_POS,80,0);
	TxDoFunction(TX_QR_ERRLEVEL,TX_QR_ERRLEVEL_L,0);
	TxDoFunction(TX_QR_DOTSIZE,8,0);
	//TxPrintQRcode(output,strlen(output));
	TxPrintQRcode(url,strlen(url));
	TxDoFunction(TX_FEED,220,0);
	TxInit();
*/
}


void CTxPrinterTestDlg::OnBnClickedButton13()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	TxDoFunction(TX_CHK_BMARK,0,0);
}


void CTxPrinterTestDlg::OnBnClickedButton14()
{
	// TODO: 在此添加控件通知处理程序代码
	TxDoFunction(TX_FEED_REV,80,0);
	TxInit();
}


void CTxPrinterTestDlg::OnBnClickedButton15()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}

	TxDoFunction(TX_CH_ROTATE,TX_CH_ROTATE_NONE,0);
	TxOutputStringLn(Chinese);
	TxDoFunction(TX_FEED,100,0);

	TxDoFunction(TX_CH_ROTATE,TX_CH_ROTATE_LEFT,0);
	TxOutputStringLn(Chinese);
	TxDoFunction(TX_FEED,100,0);

	TxDoFunction(TX_CH_ROTATE,TX_CH_ROTATE_RIGHT,0);
	TxOutputStringLn(Chinese);
	TxDoFunction(TX_FEED,220,0);

	TxInit();
}


void CTxPrinterTestDlg::OnBnClickedButton16()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	CString temp;
	using std::vector;
	vector<char> Out(6);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x77;
	Out[3] = 0x0;
	Out[4] = 0x4;
	Out[5] = 0x0;
	TxWritePrinter(&Out[0], Out.size());
	TxInit();
/*	TxDoFunction(TX_BW_REVERSE,TX_ON,0);
	TxOutputStringLn(Chinese);
	TxOutputStringLn(English);
	TxOutputStringLn(number);
	TxDoFunction(TX_FEED,220,0);
	TxInit();
*/
}


void CTxPrinterTestDlg::OnBnClickedButton17()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	CString temp;
	using std::vector;
	vector<char> Out(6);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x77;
	Out[3] = 0x0;
	Out[4] = 0x2;
	Out[5] = 0x1;
	TxWritePrinter(&Out[0], Out.size());
	TxInit();
/*	//1X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_1X,TX_SIZE_1X);
	TxOutputStringLn(Chinese);

	//2X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_2X,TX_SIZE_2X);
	TxOutputStringLn(Chinese);

	//3X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_3X,TX_SIZE_3X);
	TxOutputStringLn(Chinese);

	//4X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_4X,TX_SIZE_4X);
	TxOutputStringLn(Chinese);

	//5X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_5X,TX_SIZE_5X);
	TxOutputStringLn(Chinese);

	//6X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_6X,TX_SIZE_6X);
	TxOutputStringLn(Chinese);

	//7X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_7X,TX_SIZE_7X);
	TxOutputStringLn(Chinese);

	//8X
	TxDoFunction(TX_FONT_SIZE,TX_SIZE_8X,TX_SIZE_8X);
	TxOutputStringLn(Chinese);
	TxDoFunction(TX_FEED,220,0);

	TxInit();
*/
}


void CTxPrinterTestDlg::OnBnClickedButton19()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}

	TxDoFunction(TX_CUT,TX_CUT_PARTIAL,0);
}


void CTxPrinterTestDlg::OnBnClickedButton6()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}

	TxDoFunction(TX_CH_ROTATE,TX_CH_ROTATE_NONE,0);
	TxOutputStringLn(English);
	TxDoFunction(TX_FEED,100,0);

	TxDoFunction(TX_CH_ROTATE,TX_CH_ROTATE_LEFT,0);
	TxOutputStringLn(English);
	TxDoFunction(TX_FEED,100,0);

	TxDoFunction(TX_CH_ROTATE,TX_CH_ROTATE_RIGHT,0);
	TxOutputStringLn(English);
	TxDoFunction(TX_FEED,220,0);

	TxInit();
}


void CTxPrinterTestDlg::OnBnClickedButton20()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	CString temp;
	using std::vector;
	vector<char> Out(6);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x77;
	Out[3] = 0x0;
	Out[4] = 0x4;
	Out[5] = 0x1;
	TxWritePrinter(&Out[0], Out.size());
	TxInit();
/*	TxDoFunction(TX_SEL_FONT,TX_FONT_B,0);
	TxOutputStringLn(English);
	TxOutputStringLn(English);
	TxOutputStringLn(English);
	TxDoFunction(TX_FEED,220,0);
*/
}




void CTxPrinterTestDlg::OnBnClickedButton21()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	CString temp;
	using std::vector;
	vector<char> Out(6);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x77;
	Out[3] = 0x0;
	Out[4] = 0x5;
	Out[5] = 0x0;
	TxWritePrinter(&Out[0], Out.size());
	TxInit();
/*	TxDoFunction(TX_SEL_FONT,TX_FONT_A,0);
	TxOutputStringLn(English);
	TxOutputStringLn(English);
	TxOutputStringLn(English);
	TxDoFunction(TX_FEED,220,0);
*/
}


void CTxPrinterTestDlg::OnBnClickedButton23()
{
	// TODO: 在此添加控件通知处理程序代码
	int Result;
	CString Out;
	bool normal(true);
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	Result=TxGetStatus();
	Out.Format(_T("%x"),Result);
	Out=_T("0x")+Out+_T("\r\n");
	if (Result&TX_STAT_PAPEREND) {Out+=_T("打印机缺纸 "); normal=false;}
	if (Result&TX_STAT_BUSY) {Out+=_T("打印机繁忙"); normal=false;}
	if (normal) {Out+=_T("打印机状态正常");}
	AfxMessageBox(Out);
	//	m_Status.Format(_T("%x"),result);
	//	m_Status=_T("0x")+m_Status;

	//	UpdateData(false);
}


//void CTxPrinterTestDlg::OnDestroy()
//{
//	CDialogEx::OnDestroy();
//	TxClosePrinter();	// TODO: 在此处添加消息处理程序代码
//}


void CTxPrinterTestDlg::OnClose()
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	TxClosePrinter();
	CDialogEx::OnClose();
}


void CTxPrinterTestDlg::OnBnClickedButton27()
{
	// TODO: 在此添加控件通知处理程序代码
/*	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	CDenDlg  dlg;
	dlg.DoModal();
*/
	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }
	CString temp;
	using std::vector;
	vector<char> Out(6);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x77;
	Out[3] = 0x0;
	Out[4] = 0x2;
	Out[5] = 0x0;
	TxWritePrinter(&Out[0], Out.size());
	TxInit();
}


//void CAboutDlg::OnBnDoubleclickedButton2()
//{
// TODO: 在此添加控件通知处理程序代码
//	GetDlgItem(IDC_STATIC2)->SetWindowText("I LOVE YOU");
//	((text*)GetDlgItem(IDC_COMBO1))
//	GetDlgItem(IDC_STATIC2)->SetWindowText(_T("I LOVE YOU"));
//}


//HBRUSH CAboutDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
//{
//	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
//
//	// TODO:  在此更改 DC 的任何特性
//	if ((pWnd-> GetDlgCtrlID() == IDC_STATIC2)&eggshell) {pDC-> SetTextColor(COLORREF(RGB(127,127,0)));}
//	// TODO:  如果默认的不是所需画笔，则返回另一个画笔
//	return hbr;
//}


//void CAboutDlg::OnBnClickedButton2()
//{
// TODO: 在此添加控件通知处理程序代码
//}


//void CAboutDlg::OnBnDoubleclickedButton1000()
//{
//	// TODO: 在此添加控件通知处理程序代码
//	GetDlgItem(IDC_STATIC2)->SetWindowText(_T("I LOVE YOU"));
//}

/*
void CTxPrinterTestDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
// TODO: 在此添加消息处理程序代码和/或调用默认值
if (pScrollBar->GetDlgCtrlID()==IDC_SCROLLBAR1) 
{
int pos = page.GetScrollPos();
switch (nSBCode)
{
case SB_BOTTOM:
pos=1;
break;
case SB_LINEDOWN:
if (pos>1) {pos-=1;}
break;
case SB_LINEUP:
if (pos<500) {pos+=1;}
break;
case SB_PAGEDOWN:
if (pos>1) {pos-=1;}
break;
case SB_PAGEUP:
if (pos>1) {pos-=1;}
break;
case SB_THUMBPOSITION:
pos=nPos;
break;
default:
//SetDlgItemInt(IDC_EDIT1, pos);
mypage=pos;
UpdateData(FALSE);
GetDlgItem(IDC_EDIT1)->SendMessage(WM_PAINT);
//Invalidate();
DoEvent();
//GetDlgItem(IDC_EDIT1)->Invalidate();
return;
}
page.SetScrollPos(pos);
}
else
{
int pos=delay.GetScrollPos();
switch (nSBCode)
{
case SB_BOTTOM:
pos=0;
break;
case SB_LINEDOWN:
if (pos>0) {pos-=1;}
break;
case SB_LINEUP:
if (pos<60) {pos+=1;}
break;
case SB_PAGEDOWN:
if (pos>0) {pos-=1;}
break;
case SB_PAGEUP:
if (pos>0) {pos-=1;}
break;
case SB_THUMBPOSITION:
pos=nPos;
break;
default:
//	SetDlgItemInt(IDC_EDIT2, pos);
mydelay=pos;
UpdateData(FALSE);
GetDlgItem(IDC_EDIT2)->SendMessage(WM_PAINT);
DoEvent();
//Invalidate();
//	GetDlgItem(IDC_EDIT2)->Invalidate();
return;
}
delay.SetScrollPos(pos);
}

CDialogEx::OnVScroll(nSBCode, nPos, pScrollBar);
}
*/

/*void CTxPrinterTestDlg::OnEnChangeEdit1()
{
// TODO:  如果该控件是 RICHEDIT 控件，它将不
// 发送此通知，除非重写 CDialogEx::OnInitDialog()
// 函数并调用 CRichEditCtrl().SetEventMask()，
// 同时将 ENM_CHANGE 标志“或”运算到掩码中。
if (mypage>50) {mypage=50;}
if (mypage<1) {mypage=1;}
// TODO:  在此添加控件通知处理程序代码
}
*/

/*void CTxPrinterTestDlg::OnNMThemeChangedScrollbar1(NMHDR *pNMHDR, LRESULT *pResult)
{
// 该功能要求使用 Windows XP 或更高版本。
// 符号 _WIN32_WINNT 必须 >= 0x0501。
// TODO: 在此添加控件通知处理程序代码
*pResult = 0;
}
*/




void CTxPrinterTestDlg::DoEvent(void)
{
	MSG msg;
	while (PeekMessage(&msg, (HWND)NULL, 0, 0, PM_REMOVE) )
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

/*
void CTxPrinterTestDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
// TODO: 在此添加消息处理程序代码和/或调用默认值
//	GetDlgItem(IDC_EDIT2)->Invalidate(FALSE);
//	GetDlgItem(IDC_EDIT1)->Invalidate(FALSE);
//	CDialogEx::OnLButtonDown(nFlags, point);
}
*/
/*
void CTxPrinterTestDlg::OnEnChangeEdit2()
{
// TODO:  如果该控件是 RICHEDIT 控件，它将不
// 发送此通知，除非重写 CDialogEx::OnInitDialog()
// 函数并调用 CRichEditCtrl().SetEventMask()，
// 同时将 ENM_CHANGE 标志“或”运算到掩码中。
if (mydelay>60){mydelay=60;}
if (mydelay<0) {mydelay=0;}
// TODO:  在此添加控件通知处理程序代码
}
*/

void CTxPrinterTestDlg::OnBnClickedButton29()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	using std::vector;
	vector<char> Out(5);
	Out[0]=0x1b;
	Out[1]=0x70;
	Out[2]=0x0;
	Out[3]=0x70;
	Out[4]=0x70;
	TxWritePrinter(&Out[0],Out.size());
}


void CTxPrinterTestDlg::OnBnClickedButton30()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	using std::vector;
	vector<char> Out(5);
	Out[0]=0x1b;
	Out[1]=0x70;
	Out[2]=0x1;
	Out[3]=0x70;
	Out[4]=0x70;
	TxWritePrinter(&Out[0],Out.size());
}


void CTxPrinterTestDlg::OnBnClickedButton28()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	CString strPath;
	TCHAR fullPath[256];
	GetModuleFileName(NULL,fullPath,256);
	strPath=(CString)fullPath;
	int position=strPath.ReverseFind('\\'); 
	strPath=strPath.Left(position+1); 

	strPath += _T("test.txt");
	//CFileStatus status;
	//if (!CFile::GetStatus(strPath,status)) {AfxMessageBox(IDS_STRING112); return;}

	CFile file(strPath,CFile::modeRead);
	fLen=(DWORD)file.GetLength();
	char *buff;
	try
	{
		buff=new char[fLen+1];
		file.Read(buff,fLen);
		file.Close();
		buff[fLen]=0;
		TxWritePrinter(buff,fLen);
		delete []buff;
		TxDoFunction(TX_FEED,220,0);
		TxDoFunction(TX_CUT,TX_CUT_FULL,0);
	}
	catch (std::bad_alloc&)
	{
		AfxMessageBox(IDS_STRING109); 
		return;
	}

}


void CTxPrinterTestDlg::OnBnClickedButton31()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	UpdateData(TRUE);
	if ((page>500)|(page<1)) {AfxMessageBox(IDS_STRING113);return;}
	if ((delay>60)|(delay<0)) {AfxMessageBox(IDS_STRING114); return;}
	SetTimer(1,delay*1000,NULL);
	CString strPath;;
	TCHAR fullPath[256];
	GetModuleFileName(NULL,fullPath,256);
	strPath=(CString)fullPath;
	int position=strPath.ReverseFind('\\'); 
	strPath=strPath.Left(position+1); 
	strPath += _T("test.txt");
	//CFileStatus status;
	//if (!CFile::GetStatus(strPath,status)) {AfxMessageBox(IDS_STRING112); KillTimer(1); return;}

	CFile file(strPath,CFile::modeRead);
	fLen=(DWORD)file.GetLength();

	//char *buff;//!!!!!!!!!!!!!!!
	try
	{
		Stabuff=new char[fLen+1];
		file.Read(Stabuff,fLen);
		file.Close();
		Stabuff[fLen]=0;
		TxWritePrinter(Stabuff,fLen);
		Counter=1;
		TxDoFunction(TX_FEED,220,0);
		//buff=new char[fLen+1];
		//file.Read(buff,fLen);
		//file.Close();
		//buff[fLen]=0;
		//TxOutputStringLn(buff);
		//delete []buff;
		//TxDoFunction(TX_FEED,220,0);
		//TxDoFunction(TX_CUT,TX_CUT_FULL,0);
	}
	catch (std::bad_alloc&)
	{
		AfxMessageBox(IDS_STRING109); 
		return;
	}


}


void CTxPrinterTestDlg::OnBnClickedButton32()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
	KillTimer(1);
	TxDoFunction(TX_CUT,TX_CUT_FULL,0);
}


void CTxPrinterTestDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	TxWritePrinter(Stabuff,fLen);
	DoEvent();
	TxDoFunction(TX_FEED,220,0);
	Counter+=1;
	if (Counter==page) {KillTimer(1);TxDoFunction(TX_CUT,TX_CUT_FULL,0); return;}
	

	CDialogEx::OnTimer(nIDEvent);
}


//
//{
//	CPaintDC dc(this); // device context for painting
// TODO: 在此处添加消息处理程序代码
// 不为绘图消息调用 CDialogEx::OnPaint()
//}


void CTxPrinterTestDlg::OnEnChangeEditPage()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
	//UpdateData(TRUE);
	//if (page>500) {page=500; UpdateData(FALSE);}
	//if (page<1) {page=1; UpdateData(FALSE);}
}


void CTxPrinterTestDlg::OnEnChangeEditDelay()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
	//UpdateData(TRUE);
	//if (delay>60) {delay=60; UpdateData(FALSE);}
	//if (delay<0) {delay=1; UpdateData(FALSE);}
}

BOOL CTxPrinterTestDlg::PreTranslateMessage(MSG *pMsg)
{
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)  return TRUE;
	if(pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN) return TRUE; 
	else 
		return CDialog::PreTranslateMessage(pMsg);
}

void CTxPrinterTestDlg::OnCbnSelchangeCombo3()
{
	// TODO: 在此添加控件通知处理程序代码
	
	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }
		int baud;
		TxInit();
		baud = ((CComboBox*)GetDlgItem(IDC_COMBO3))->GetCurSel();
		if ((baud < 0) || (baud>7))  AfxMessageBox(IDS_STRING116);
		using std::vector;
		vector<char> Out(6);
		Out[0] = 0x1b;
		Out[1] = 0x7c;
		Out[2] = 0x77;
		Out[3] = 0x0;
		Out[4] = 0x8;
		Out[5] = baud;
		/*switch (baud)
		{
		case 0: Out[5] = 0x0; break;
		case 1: Out[5] = 0x1; break;
		case 2: Out[5] = 0x2; break;
		case 3: Out[5] = 0x3; break;
		case 4: Out[5] = 0x4; break;
		case 5: Out[5] = 0x5; break;
		case 6: Out[5] = 0x6; break;
		case 7: Out[5] = 0x7; break;

		default:break;

		}*/
		TxWritePrinter(&Out[0], Out.size());
		AfxMessageBox(IDS_STRING117);
		TxInit();
}


//void CTxPrinterTestDlg::OnBnClickedButton33()
//{
//	// TODO: Add your control notification handler code here
//	//int Result;
//	//CString Out;
//	//bool normal(true);
//	char buf[1];
//	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }
//	using std::vector;
//	vector<char> Out(3);
//	Out[0] = 0x10;
//	Out[1] = 0x4;
//	Out[2] = 0x4;
//	TxWritePrinter(&Out[0], Out.size());
//	TxReadPrinter(buf, 1);
//	CString Temp;
//	Temp.Format(_T("%X"), buf[0]);
//	Temp = _T("0x") + Temp;
//	AfxMessageBox(Temp);
//    
//	//result=TxGetStatus2();
//	//Out.Format(_T("%x"), Result);
//	//Out = _T("0x") + Out + _T("\r\n");
//	//if (Result&TX_STAT_PAPEREND) { Out += _T("打印机缺纸 "); normal = false; }
//	//if (Result&TX_STAT_BUSY) { Out += _T("打印机繁忙"); normal = false; }
//	//if (Result&TX_STAT_COVER) { Out += _T("打印机机芯的盖子打开 "); normal = false; }
//	//if (Result&TX_STAT_ERROR) { Out += _T("打印机错误 "); normal = false; }
//	//if (Result&TX_STAT_RCV_ERR) { Out += _T("有需人工恢复的可恢复错误 "); normal = false; }
//	//if (Result&TX_STAT_CUT_ERR) { Out += _T("有切刀错误 "); normal = false; }
//	//if (Result&TX_STAT_URCV_ERR) { Out += _T("有不可恢复错误 "); normal = false; }
//	//if (Result&TX_STAT_ARCV_ERR) { Out += _T("有可自动恢复的错误 "); normal = false; }
//	//if (Result&TX_STAT_PAPER_NE) { Out += _T("纸将近 "); normal = false; }
//	//if (normal) { Out += _T("打印机状态正常"); }
//	//AfxMessageBox(Out);
//
//}


//void CTxPrinterTestDlg::OnBnClickedButton34()
//{
//	// TODO: 在此添加控件通知处理程序代码
//	if (ifopen==false) {AfxMessageBox(IDS_STRING107); return;}
//
//	TxDoFunction(TX_CUT, TX_PURECUT_FULL,0);
//}


//void CTxPrinterTestDlg::OnBnClickedButton35()
//{
//	// TODO: 在此添加控件通知处理程序代码
//	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }
//
//	TxDoFunction(TX_CUT, TX_PURECUT_PARTIAL, 0);
//}


void CTxPrinterTestDlg::OnBnClickedButton36()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }
	CString temp;
	using std::vector;
	vector<char> Out(6);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x77;
	Out[3] = 0x0;
	Out[4] = 0x5;
	Out[5] = 0x1;
	TxWritePrinter(&Out[0], Out.size());
	TxInit();
}


void CTxPrinterTestDlg::OnBnClickedButton37()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }
	CString temp;
	using std::vector;
	vector<char> Out(6);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x77;
	Out[3] = 0x0;
	Out[4] = 0x7;
	Out[5] = 0x0;
	TxWritePrinter(&Out[0], Out.size());
	TxInit();
}


void CTxPrinterTestDlg::OnBnClickedButton38()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }
	CString temp;
	using std::vector;
	vector<char> Out(6);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x77;
	Out[3] = 0x0;
	Out[4] = 0x7;
	Out[5] = 0x1;
	TxWritePrinter(&Out[0], Out.size());
	TxInit();
}


void CTxPrinterTestDlg::OnBnClickedButton39()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }
	CString temp;
	using std::vector;
	vector<char> Out(6);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x77;
	Out[3] = 0x0;
	Out[4] = 0x6;
	Out[5] = 0x0;
	TxWritePrinter(&Out[0], Out.size());
	TxInit();
}


void CTxPrinterTestDlg::OnBnClickedButton40()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }
	CString temp;
	using std::vector;
	vector<char> Out(6);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x77;
	Out[3] = 0x0;
	Out[4] = 0x6;
	Out[5] = 0x1;
	TxWritePrinter(&Out[0], Out.size());
	TxInit();
}


//void CTxPrinterTestDlg::OnBnClickedButton41()
//{
//	// TODO: 在此添加控件通知处理程序代码
//	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }
//	int baud;
//	TxInit();
//	baud = ((CComboBox*)GetDlgItem(IDC_COMBO3))->GetCurSel();
//	if ((baud < 0) || (baud>7))  AfxMessageBox(IDS_STRING116);
//	using std::vector;
//	vector<char> Out(6);
//	Out[0] = 0x1b;
//	Out[1] = 0x7c;
//	Out[2] = 0x77;
//	Out[3] = 0x0;
//	Out[4] = 0x8;
//	switch (baud)
//	{
//	case 0: Out[5] = 0x0; break;
//	case 1: Out[5] = 0x1; break;
//	case 2: Out[5] = 0x2; break;
//	case 3: Out[5] = 0x3; break;
//	case 4: Out[5] = 0x4; break;
//	case 5: Out[5] = 0x5; break;
//	case 6: Out[5] = 0x6; break;
//	case 7: Out[5] = 0x7; break;
//
//	default:break;
//	}
////	if (code == 0) { Out[9] = 0x0; }
////	if (code == 1) { Out[9] = 0x1; }
//	TxWritePrinter(&Out[0], Out.size());
//	AfxMessageBox(IDS_STRING117);
//	TxInit();
//}


void CTxPrinterTestDlg::OnCbnSelchangeCombo4()
{
	// TODO: 在此添加控件通知处理程序代码int code;
	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }
	int CharDoubleWay;
	TxInit();
	CharDoubleWay = ((CComboBox*)GetDlgItem(IDC_COMBO4))->GetCurSel();
	if ((CharDoubleWay < 0) || (CharDoubleWay>19)) AfxMessageBox(IDS_STRING119);
	using std::vector;
	vector<char> Out(6);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x77;
	Out[3] = 0x0;
	Out[4] = 0x3;
	Out[5] = CharDoubleWay+1;
	TxWritePrinter(&Out[0], Out.size());
	
	Out[0] = 0x1b;
	Out[1] = 0x33;
	Out[2] = 0x10;
	TxWritePrinter(&Out[0], 3);

	TxOutputStringLn("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
	TxOutputStringLn("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
	
	TxNewline();
	TxNewline();
	TxNewline();
	TxNewline();
	TxNewline();
	TxNewline();
	
	Out[0] = 0x1b;
	Out[1] = 0x33;
	Out[2] = 0x18;
	TxWritePrinter(&Out[0], 3);

	AfxMessageBox(IDS_STRING115);
	TxInit();
}


//HBRUSH CTxPrinterTestDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
//{
//	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
//	if (pWnd->GetDlgCtrlID() == IDC_STATIC1)
//	{
//		pDC->SetTextColor(RGB(255, 0, 0));
//	}
//
//	// TODO:  在此更改 DC 的任何特性
//
//	// TODO:  如果默认的不是所需画笔，则返回另一个画笔
//	return hbr;
//}


void CTxPrinterTestDlg::OnCbnSelchangeCombo5()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }
	int code;
	code = ((CComboBox*)GetDlgItem(IDC_COMBO5))->GetCurSel();
	using std::vector;
	vector<char> Out(6);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x77;
	Out[3] = 0x0;
	Out[4] = 0x11;
	Out[5] = code;
	TxWritePrinter(&Out[0], Out.size());
	AfxMessageBox(IDS_STRING120);


	
}




void CTxPrinterTestDlg::OnCbnSelchangeCombo6()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }
	int code;
	code = ((CComboBox*)GetDlgItem(IDC_COMBO6))->GetCurSel();
	using std::vector;
	vector<char> Out(6);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x77;
	Out[3] = 0x0;
	Out[4] = 0x9;
	Out[5] = code;
	TxWritePrinter(&Out[0], Out.size());
	AfxMessageBox(IDS_STRING121);
}


void CTxPrinterTestDlg::OnCbnSelchangeCombo7()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }
	int code;
	code = ((CComboBox*)GetDlgItem(IDC_COMBO7))->GetCurSel();
	using std::vector;
	vector<char> Out(6);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x77;
	Out[3] = 0x0;
	Out[4] = 0x14;
	Out[5] = code;
	TxWritePrinter(&Out[0], Out.size());
	AfxMessageBox(IDS_STRING122);
}


void CTxPrinterTestDlg::OnCbnSelchangeCombo8()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }
	int code;
	code = ((CComboBox*)GetDlgItem(IDC_COMBO8))->GetCurSel();
	using std::vector;
	vector<char> Out(6);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x77;
	Out[3] = 0x0;
	Out[4] = 0x13;
	Out[5] = code;
	TxWritePrinter(&Out[0], Out.size());
	AfxMessageBox(IDS_STRING123);
}


void CTxPrinterTestDlg::OnCbnSelchangeCombo9()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }
	int code;
	code = ((CComboBox*)GetDlgItem(IDC_COMBO9))->GetCurSel();
	using std::vector;
	vector<char> Out(6);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x77;
	Out[3] = 0x0;
	Out[4] = 0x12;
	Out[5] = code;
	TxWritePrinter(&Out[0], Out.size());
	AfxMessageBox(IDS_STRING124);
}


void CTxPrinterTestDlg::OnBnClickedButton33()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }
	using std::vector;
	vector<char> Out(5);
	Out[0] = 0x1b;
	Out[1] = 0x7c;
	Out[2] = 0x0;
	Out[3] = 0xFF;
	Out[4] = 0xFF;
	TxWritePrinter(&Out[0], Out.size());
	AfxMessageBox(IDS_STRING125);
}


void CTxPrinterTestDlg::OnBnClickedButton34()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }

	TxDoFunction(TX_CUT, TX_PURECUT_FULL, 0);
}


void CTxPrinterTestDlg::OnBnClickedButton35()
{
	// TODO: 在此添加控件通知处理程序代码
	if (ifopen == false) { AfxMessageBox(IDS_STRING107); return; }

	TxDoFunction(TX_CUT, TX_PURECUT_PARTIAL, 0);
}

