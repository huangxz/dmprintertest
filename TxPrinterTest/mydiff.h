#define TX_TYPE_NONE       0  
#define TX_TYPE_USB        1   //这个是USB口
#define TX_TYPE_LPT        2   //这个是并口
#define TX_TYPE_COM        3   //这个是串口

#define TX_STAT_NOERROR    0x0008   //无故障
#define TX_STAT_SELECT     0x0010   //处于联机状态
#define TX_STAT_PAPEREND   0x0020   //缺纸
#define TX_STAT_BUSY       0x0080   //繁忙

#define TX_STAT_DRAW_HIGH  0x0100   //钱箱接口的电平（整机使用的，模块无用）
#define TX_STAT_COVER      0x0200   //打印机机芯的盖子打开
#define TX_STAT_ERROR      0x0400   //打印机错误
#define TX_STAT_RCV_ERR    0x0800   //可恢复错误（需要人工干预）
#define TX_STAT_CUT_ERR    0x1000   //切刀错误
#define TX_STAT_URCV_ERR   0x2000   //不可恢复错误
#define TX_STAT_ARCV_ERR   0x4000   //可自动恢复的错误
#define TX_STAT_PAPER_NE   0x8000   //快要没有纸了

#define TX_SER_BAUD_MASK   0xFF000000   //波特率 
#define TX_SER_BAUD9600    0x00000000   //9600的波特率
#define TX_SER_BAUD19200   0x01000000   //19200的波特率
#define TX_SER_BAUD38400   0x02000000   //38400的波特率
#define TX_SER_BAUD57600   0x03000000   //57600的波特率
#define TX_SER_BAUD115200  0x04000000   //115200的波特率

#define TX_SER_DATA_MASK   0x00FF0000   //数据位 
#define TX_SER_DATA_8BITS  0x00000000   //8位数据位
#define TX_SER_DATA_7BITS  0x00010000   //7为数据位

#define TX_SER_PARITY_MASK 0x0000FF00   //校验 
#define TX_SER_PARITY_NONE 0x00000000   //无校验
#define TX_SER_PARITY_EVEN 0x00000100   //偶校验
#define TX_SER_PARITY_ODD  0x00000200   //奇校验

#define TX_SER_STOP_MASK   0x000000F0   //停止位 
#define TX_SER_STOP_1BITS  0x00000000   //1位停止位
#define TX_SER_STOP_15BITS 0x00000010   //1.5位停止位
#define TX_SER_STOP_2BITS  0x00000020   //2位停止位

#define TX_SER_FLOW_MASK   0x0000000F   //流控制 
#define TX_SER_FLOW_NONE   0x00000000   //无流控
#define TX_SER_FLOW_HARD   0x00000001   //硬件流控（DTR/DSR方式）
#define TX_SER_FLOW_SOFT   0x00000002   //软件流控（XON/XOFF方式）

#define TX_BAR_UPCA        65
#define TX_BAR_UPCE        66
#define TX_BAR_EAN13       67
#define TX_BAR_EAN8        68
#define TX_BAR_CODE39      69
#define TX_BAR_ITF         70
#define TX_BAR_CODABAR     71
#define TX_BAR_CODE93      72
#define TX_BAR_CODE128     73

#define TX_FONT_SIZE       1 //控制字体的放大倍数功能，后面的2个参数0为正常大小，递增1为增大1倍，如此类推，最大为7；参数1(param1)为字体宽的倍数，参数2(param2)为字体高的倍数

#define TX_FONT_ULINE      2  //控制是否有下划线功能，只需1个参数，param1=TX_ON(有下划线)，=TX_OFF(无小划线）,param2=0就可以了

#define TX_FONT_BOLD       3  //控制是否粗体功能，只需1个参数，param1=TX_ON(使用粗体)，=TX_OFF(不使用粗体),param2=0就可以了

#define TX_SEL_FONT        4  //选择英文字体功能，也是只需要1个参数，TX_FONT_A(12X24)或TX_FONT_B(9X17),param2=0就可以了

#define TX_FONT_ROTATE     5  //字体是否旋转90度功能，只需要1个参数

#define TX_ALIGN           6  //控制对齐功能，参数为TX_ALIGN_XXX，只需要1个参数。
#define TX_ALIGN_LEFT      0  //左对齐的设置参数
#define TX_ALIGN_CENTER    1  //对中设置的参数
#define TX_ALIGN_RIGHT     2  //右对齐的设置参数

#define TX_CHINESE_MODE    7  //中文/英文模式切换功能，只需要1个参数

#define TX_FEED            10 //执行走纸功能，只需要1个参数，参数以毫米为单位

#define TX_UNIT_TYPE       11 //设置动作单位(无效)

#define TX_CUT             12 //执行切纸功能，第一参数指明类型，第二参数指明切纸前的走纸距离
#define TX_CUT_FULL        0  //全切的设置参数
#define TX_CUT_PARTIAL     1  //半切的设置参数

#define TX_HOR_POS         13 //绝对水平定位功能，只需要1个参数，参数以毫米为单位

#define TX_LINE_SP         14 //设置行间距功能，只需要1个参数，参数是以点数为单位的

#define TX_BW_REVERSE      15 //设置字体是否黑白翻转功能，只需要1个参数

#define TX_UPSIDE_DOWN     16 //设置是否倒置打印功能，只需要1个参数

#define TX_INET_CHARS      17 //选择国际字符集功能，只需要1个参数,通常这个设置也是在英文方式下使用的，只是针对12个特定的ASCII码不同国家的使用的字形不同，默认是0，表示是使用美国的字符集

#define TX_CODE_PAGE       18 //选择字符代码页功能，只需要1个参数，通常是0~n,表示选择的代码页参数，祥见打印机说明书一般默认是0，是表示PC437的代码页，这个功能要只在英文方式下有效

#define TX_CH_ROTATE       19 //设定汉字旋转功能，只需要1个参数，可以表示3种选择，如下所示：
#define TX_CH_ROTATE_NONE  0  //不旋转
#define TX_CH_ROTATE_LEFT  1  //向左旋转
#define TX_CH_ROTATE_RIGHT 2  //向右旋转

#define TX_CHK_BMARK       20 //寻找黑标黑标TX_CHK_BMARK是执行黑标检测，这个动作本身是不需要参数的，

#define TX_SET_BMARK       21 //设置黑标相关偏移量功能

#define TX_PRINT_LOGO      22 //打印已下载好的LOGO的功能，只需1个参数，可以表示4种选择，如下所示：
#define TX_LOGO_1X1        0  //对应203X203的点密度，就是正常大小。
#define TX_LOGO_1X2        1  //对应203x101的点密度，就是LOG在水平方向上放大到了2倍。
#define TX_LOGO_2X1        2  //对应101x203的点密度，就是LOG在垂直方向上放大到了2倍。
#define TX_LOGO_2X2        3  //对应101x101的点密度，就是LOG在水平和垂直方向上放大到了2倍。'

#define TX_BARCODE_HEIGHT  23 //设定条码高度功能,只需要1个参数,单位为点数

#define TX_BARCODE_WIDTH   24 //设定条码宽度功能,只需要1个参数,单位为点数, 不能小于2

#define TX_BARCODE_FONT    25 //选择条码HRI字符的打印位置,只需要1个参数，可以表示4种选择，如下所示：
#define TX_BAR_FONT_NONE   0  //不打印
#define TX_BAR_FONT_UP     1  //打印在条码的上面
#define TX_BAR_FONT_DOWN   2  //打印在条码的下面
#define TX_BAR_FONT_BOTH   3  //条码的上面下面都打印

#define TX_FEED_REV        26 //执行反向走纸功能，只需要1个参数，参数以毫米为单位

#define TX_ON              1  //功能设置有效的参数
#define TX_OFF             0  //功能设置无效的参数

#define TX_FONT_A          0  //12X24点阵的设置参数
#define TX_FONT_B          1  //9X17点阵的设置参数

#define TX_SIZE_1X         0  //正常大小
#define TX_SIZE_2X         1  //2倍大小
#define TX_SIZE_3X         2  //3倍大小
#define TX_SIZE_4X         3  //4倍大小
#define TX_SIZE_5X         4  //5倍大小
#define TX_SIZE_6X         5  //6倍大小
#define TX_SIZE_7X         6  //7倍大小
#define TX_SIZE_8X         7  //8倍大小

#define TX_UNIT_PIXEL      0  //对应TX_UNIT_TYPE
#define TX_UNIT_MM         1

#define TX_BM_START        1  //起始打印位置相对于黑标检测位置的偏移量
#define TX_BM_TEAR         2  //切/撕纸位置相对于黑标检测位置的偏移量







