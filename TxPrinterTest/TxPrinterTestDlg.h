
// TxPrinterTestDlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include "export.h"
#include "SendHexData.h"
#include "DenDlg.h"

// CTxPrinterTestDlg 对话框
class CTxPrinterTestDlg : public CDialogEx
{
// 构造
public:
	CTxPrinterTestDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_TXPRINTERTEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
//	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
//	afx_msg void OnCbnSelchangeCombo1();
//	afx_msg void OnCbnSelchangeCombo1();
	afx_msg void OnBnClickedButton25();
private:
	virtual BOOL PreTranslateMessage(MSG *pMsg);

public:
	DWORD m_port;
public:
	DWORD_PTR m_portIdx;

public:
	DWORD m_baud;
public:
	afx_msg void OnBnClickedButton1();
	bool ifopen;
	afx_msg void OnBnClickedButton2();

//public:
//	CString m_Status;
public:
	afx_msg void OnBnClickedButton26();
	char* Chinese;
	char* English;
	char* number; 
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton18();
	
	afx_msg void OnBnClickedButton24();
	afx_msg void OnBnClickedButton9();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton22();
	afx_msg void OnBnClickedButton7();
	afx_msg void OnBnClickedButton8();
	afx_msg void OnBnClickedButton10();
	afx_msg void OnBnClickedButton11();
	afx_msg void OnBnClickedButton12();
	afx_msg void OnBnClickedButton13();
	afx_msg void OnBnClickedButton14();
	afx_msg void OnBnClickedButton15();
	afx_msg void OnBnClickedButton16();
	afx_msg void OnBnClickedButton17();
	afx_msg void OnBnClickedButton19();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton20();
	//afx_msg void OnBnClickedButton21();
	afx_msg void OnBnClickedButton21();
	afx_msg void OnBnClickedButton23();
//	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnBnClickedButton27();
	//CScrollBar page;
	//CScrollBar delay;
	//afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//int mypage;
	//int mydelay;
	//afx_msg void OnEnChangeEdit1();
	//afx_msg void OnNMThemeChangedScrollbar1(NMHDR *pNMHDR, LRESULT *pResult);
	//int mypage;
	//int mydelay;
	void DoEvent(void);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//afx_msg void OnEnChangeEdit2();
	afx_msg void OnBnClickedButton29();
	afx_msg void OnBnClickedButton30();
	afx_msg void OnBnClickedButton28();
	afx_msg void OnBnClickedButton31();
	afx_msg void OnBnClickedButton32();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	int Counter;
	char* Stabuff;
	int delay;
	int page;
	afx_msg void OnPaint();
	afx_msg void OnEnChangeEditPage();
	afx_msg void OnEnChangeEditDelay();
	DWORD fLen;
	afx_msg void OnCbnSelchangeCombo3();
	//afx_msg void OnBnClickedButton33();
	//afx_msg void OnBnClickedButton34();
	//afx_msg void OnBnClickedButton35();
	//afx_msg void OnBnClickedButton36();
	//afx_msg void OnBnClickedButton37();
	//afx_msg void OnBnClickedButton38();
	//afx_msg void OnBnClickedButton39();
	//afx_msg void OnBnClickedButton40();
	//afx_msg void OnBnClickedButton41();
	//afx_msg void OnCbnSelchangeCombo4();
//	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnCbnSelchangeCombo5();
	afx_msg void OnCbnSelchangeCombo4();
	afx_msg void OnCbnSelchangeCombo6();
	afx_msg void OnCbnSelchangeCombo7();
	afx_msg void OnCbnSelchangeCombo8();
	afx_msg void OnCbnSelchangeCombo9();
	afx_msg void OnBnClickedButton33();
	afx_msg void OnBnClickedButton34();
	afx_msg void OnBnClickedButton35();
	afx_msg void OnBnClickedButton39();
	afx_msg void OnBnClickedButton40();
	afx_msg void OnBnClickedButton37();
	afx_msg void OnBnClickedButton38();
	afx_msg void OnBnClickedButton36();
};
